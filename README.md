# iDAM_Prueba_Tecnica

En este repositorio se encuentran los desarrollos realizados para completar los objetivos propuestos por la prueba técnica de desarrollador Django de la empresa iDAM.

*Nota: La plataforma de despliegue escogida para los desarrollos del presente trabajo es "pythonanywhere", esto debido a su facilidad de implementación opción de gratuidad*

## Plataforma de Login

El desarrollo de la plataforma se encuentra registrado en la carpeta **django_login**, en esta se encuentran dos aplicaciones **baseapp** y **loginapp**. La primera siendo la que maneja la pantalla tras un acceso correcto, la segunda maneja la plataforma de login.

El funcionamiento básico de esta plataforma se basa en permitir el ingreso de un super usuario, identificado con credenciales

* Usuario: admin
* Contraseña: 123456

Y negar el ingreso (mediante a un mensaje de error) a todo aquel que no cumpla los requerimientos. Esta comprobación de requerimientos se hace con el uso del propio servicio de login ofrecido por Django en su rutina *django.contrib.auth*. De esta misma rutina se pueden implementar plataformas de registro y logout compatibles con esta plataforma.

La aplicación desplegada se puede encontrar en [este link](http://pabloortiz.pythonanywhere.com/).

## Página web

Para este punto se ha decidido hacer una apuesta arriesgada en el concepto "blog personal", implementando un sistema de recomendación de música, basado en la teoría del color. Para esto se relaciona una emoción con un color y una canción, y se deja que el usuario (desde la ingenuidad) escoja el color que representa lo que siente, para así presentarle una canción que represente ese sentimiento bajo la opinión del desarrollador.

Este desarrollo se encuentra registrado en la carpeta **django_blog**, en esta se encuentra una única aplición que maneja la comunicación entre pantallas, habiendo una pantalla por canción adicional a la pantalla principal y un *About Us*.

Para esta plataforma se utiliza la librería *embed_video* del propio Django para presentar los videos de YouTube de las canciones previamente mencionadas. El cómo aplicar esta librería se encuentra en los archivos *models.py* y *admin.py*. Para esto los videos se añaden a la plataforma a mano desde el *path* "admin/" y luego se añaden a la página en el *template* con el uso de la clase **Video**.

La aplicación desplegada se puede encontrar en [este link](http://juanportiz.pythonanywhere.com/).
