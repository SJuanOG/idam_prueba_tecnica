from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect

@csrf_protect
def login_user(request):
    #verification of username and password given. this process was found on Django documentation
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('logged/')  #redirection to new page if login is successful
        else:
            messages.success(request, "There was an error loggin in, try again") #Error message to display
            return render(request, 'registration/login.html', {})                #Reload of the page to attempt a new login
    
    else:
        return render(request, 'registration/login.html', {})                    #First page to show at arrival
