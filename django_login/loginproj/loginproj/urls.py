
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('loginapp.urls')),            #Main pago for login
    path('', include('django.contrib.auth.urls')), #Django auth system
    path('logged/',include('baseapp.urls'), name = 'base') #Page to be redirected when logged in
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
