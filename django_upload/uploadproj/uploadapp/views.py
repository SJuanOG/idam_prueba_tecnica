from django.http import HttpResponse
from django.shortcuts import render
from .forms import UploadFileForm
from django.forms import formset_factory
from django import forms

def home(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        file = request.FILES.get('file',False)
        return HttpResponse('The name is '+ str(file))
    else:
        form = UploadFileForm()
    return render(request, 'home.html', {'form': form})
