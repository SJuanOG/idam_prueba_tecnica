from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

#URL's used in the implementation'
urlpatterns = [
    path('', views.home, name='home' ),
    path('blue-mood/', views.support, name='support'),
    path('about-us/', views.about, name='about'),
    path('orange-mood/', views.contact, name='contact'),
    path('green-mood/', views.other, name='other')
]

urlpatterns += staticfiles_urlpatterns()  #staticfiles taken into accont (css and images)

 
