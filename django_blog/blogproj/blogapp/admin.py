from django.contrib import admin
from .models import Video

#Admin site video section, to add the videos manually
admin.site.register(Video)
