from django.shortcuts import render
from .models import Video

#In this file the html pages are managed, every video configured in the admin section is assigned to its specific page

def home(request):
    return render(request, 'index.html', {})

def support(request):
    videos = Video.objects.all()
    videos = videos[2]
    return render(request, 'support.html',context = {'videos': videos})

def about(request):
    return render(request, 'about.html',{})

def contact(request):
    videos = Video.objects.all()
    videos = videos[1]
    return render(request, 'contact.html',context={'videos': videos})

def other(request):
    videos = Video.objects.all()
    videos = videos[0]
    return render(request, 'other.html',context={'videos': videos})
